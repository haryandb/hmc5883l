import smbus
import math
import sys

from hmc5883l import *
from Tkinter import *

class frmMain:
    def __init__(self, root):
	self.root = root
        self.root.title("Kompas")
	self.canvas = Canvas(root)
	self.canvas.pack()
	self.canvas.pack(expand=YES, fill=BOTH)
        self.canvas.config(width = 700,height = 500)
	self.compassCircle = self.canvas.create_oval(200,0,700,500)
	self.canvas.itemconfigure(self.compassCircle, fill = 'blue')
	self.gyroCircle = self.canvas.create_oval(425,475,475,525)
	self.canvas.itemconfigure(self.gyroCircle,fill = 'red')
	self.needle = self.canvas.create_line(450,250,450,250,fill = 'yellow',width=6)
		
	self.textHeadingCaption = self.canvas.create_text(95,20, text = 'Kompas')
	self.textHeading = self.canvas.create_text(100,40, text = '360',font = ('courier', 32, 'bold'))
	self.update()
	self.root.mainloop()

    def update(self):
        heading = int(compass.heading())
        self.canvas.itemconfigure(self.textHeading, text = str(heading))
        heading = math.radians(heading)
        x, y = int(math.cos(heading)*250), int(math.sin(heading)*250)
        self.canvas.coords(self.needle,450,250,450 + x,250 + y)
		
  	x = read_word_2c(0x43)
	x = int(x/13) - 1010
	y = read_word_2c(0x45)
	y = int(y/13) - 252
	z = read_word_2c(0x47)
	z = int(z/13)
        print "{} {} {}".format(x,y,z)
	self.canvas.coords(self.gyroCircle, 425 + x,475 + y, 475 + x, 525 +y)
        self.root.after(1000,self.update)
			
			
power_mgnt_1 =0x6b
power_mgnt_2 =0x6c

def read_byte(adr):
    return compass.bus.read_byte_data(address, adr)

def read_word(adr):
    high = compass.bus.read_byte_data(address, adr)
    low = compass.bus.read_byte_data(address,adr +1)
    val = (high<<8) + low
    return val
 
def read_word_2c(adr):
    val = read_word(adr)
    if (val >= 0x8000):
        return -((65535 - val) + 1)
    else:
        return val


compass = hmc5883l(gauss = 4.7, declination = (-2,5))
address = compass.address
root = Tk()
frmMain(root)
